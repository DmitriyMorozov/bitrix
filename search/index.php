<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница поиска");
?><?$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "",
    Array(
        "RESTART" => "Y",
        "CHECK_DATES" => "N",
        "USE_TITLE_RANK" => "N",
        "DEFAULT_SORT" => "rank",
        "arrFILTER" => array(
            0 => "forum",
            1 => "iblock_news",
            2 => "iblock_products",
            3 => "iblock_vacancies",
            4 => "iblock_Stocks",
            5 => "iblock_reviews",
            6 => "iblock_rest_entity",
            7 => "blog",
            8 => "microblog",
        ),
        "SHOW_WHERE" => "Y",
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => "5",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "arrows",
        "USE_SUGGEST" => "Y",
        "SHOW_ITEM_TAGS" => "N",
        "SHOW_ITEM_DATE_CHANGE" => "N",
        "SHOW_ORDER_BY" => "N",
        "SHOW_TAGS_CLOUD" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "template1",
        "NO_WORD_LOGIC" => "Y",
        "FILTER_NAME" => "",
        "USE_LANGUAGE_GUESS" => "Y",
        "SHOW_RATING" => "",
        "RATING_TYPE" => "",
        "PATH_TO_USER_PROFILE" => "",
        "arrFILTER_forum" => array(
            0 => "all",
        ),
        "arrFILTER_main" => "",
        "arrFILTER_iblock_news" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_products" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_vacancies" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_Stocks" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_reviews" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_rest_entity" => array(
            0 => "all",
        ),
        "arrFILTER_blog" => array(
            0 => "all",
        ),
        "arrWHERE" => array(
            0 => "forum",
            1 => "iblock_news",
            2 => "iblock_products",
            3 => "iblock_vacancies",
            4 => "iblock_Stocks",
            5 => "iblock_reviews",
            6 => "iblock_rest_entity",
            7 => "blog",
        )

    )
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>