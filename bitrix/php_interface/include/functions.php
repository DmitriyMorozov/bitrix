<?
function test_dump($var, $die = false)
{
    if (currentUserIsAdmin()) {
        echo '<font style="text-align: left; font-size: 10px"><pre>';
        var_dump($var);
        echo '</pre></font><br>';
    }

    if ($die) {
        die;
    }
}

function currentUserIsAdmin()
{
    global $USER;
    return $USER->IsAdmin();
}
?>