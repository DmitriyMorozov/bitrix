<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) return "";

$strReturn = '';

$strReturn .= '<div class="bc_breadcrumbs">
    <ul>';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $arrow = ($index > 0 ? '<i class="fa fa-angle-right"></i>' : '');

    if ($arResult[$index]["LINK"] != "" && $index != $itemSize - 1) {
        $strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            '.$arrow.'
            <a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">
                <span itemprop="name">'.$title.'</span>
            </a>
            <meta itemprop="position" content="'.($index + 1).'" />
        </li>';
    } else {
        $strReturn .= '<li>
            '.$arrow.'
            <span>'.$title.'</span>
        </li>';
    }
}

$strReturn .= '</ul>
    <div class="clearboth"></div>
</div>';

return $strReturn;
?>