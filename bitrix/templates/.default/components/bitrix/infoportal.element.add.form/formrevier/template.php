<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="review-feedback-form-wrap">
    <div class="review-feedback-form-title">Оставьте свой отзыв</div>
    <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data" class="review-feedback-form">
        <?=bitrix_sessid_post()?>
        <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
        <div class="review-feedback-field-wrap">
            <span class="review-feedback-field">
                <span class="review-feedback-field-title">Имя и фамилия</span>
                <input class="review-feedback-inp" type="text" name="PROPERTY[NAME][0]" size="25" value=""></span><span class="review-feedback-field"><span class="review-feedback-field-title">Должность/компания</span>
                <input class="review-feedback-inp" type="text" name="PROPERTY[PREVIEW_TEXT][0]" size="25" value="">
            </span>
        </div>

        <div class="review-feedback-text">
            <div class="review-feedback-text-title">Текст отзыва</div>
            <textarea name="PROPERTY[DETAIL_TEXT][0]" cols="30" rows="10" class="review-feedback-text-field"></textarea>
        </div>

        <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
            <div>
                <?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                <?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:
                <input type="text" name="captcha_word" maxlength="50" value="">
            </div>
        <?endif?>

        <div class="review-feedback-btn-block">
            <input type="submit" name="iblock_submit" value="Оставить свой отзыв" class="review-feedback-btn">
        </div>
    </form>
</div>