<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult)):
    ?>
    <div class="sb_nav">
        <ul>
            <?php
            $previousLevel = 0;
            foreach ($arResult as $arItem):
            if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
                echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
            }

            if ($arItem["IS_PARENT"]) {
            if ($arItem["DEPTH_LEVEL"] == 1) {
            ?>
            <li class="close">
                <a href="<?= $arItem["LINK"] ?>">
                    <span><?= $arItem["TEXT"] ?></span>
                </a>
                <span class="sb_showchild"></span>
                <ul>
                    <?
                    } else {
                    ?>
                    <li class="close">
                        <a href="<?= $arItem["LINK"] ?>"><span><?= $arItem["TEXT"] ?></span></a>
                        <span class="sb_showchild"></span>
                        <ul>
                            <?
                            }
                            } else {
                                if ($arItem["PERMISSION"] > "D") {
                                    if ($arItem["DEPTH_LEVEL"] == 1) {
                                        ?>
                                        <li class="close">
                                            <a href="<?= $arItem["LINK"] ?>">
                                                <span><?= $arItem["TEXT"] ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li class="close">
                                            <a href="<?= $arItem["LINK"] ?>">
                                                <span><?= $arItem["TEXT"] ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                            }

                            $previousLevel = $arItem["DEPTH_LEVEL"];
                            endforeach;

                            if ($previousLevel > 1) {
                                echo str_repeat("</ul></li>", ($previousLevel - 1));
                            }
                            ?>
                        </ul>
    </div>
<?
endif;
?>