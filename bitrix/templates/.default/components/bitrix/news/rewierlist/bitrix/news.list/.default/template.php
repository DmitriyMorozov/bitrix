<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>

<div class="news-list">
    <?php if($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <?php endif; ?>

    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="review-block" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="review-text">
                <div class="review-block-title">
                    <span class="review-block-name">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                    </span>
                    <span class="review-block-description"><?= $arItem["PREVIEW_TEXT"] ?></span>
                </div>
                <div class="review-text-cont">
                    <?= $arItem["DETAIL_TEXT"] ?>
                </div>
            </div>
            <div class="review-img-wrap">
                <a href="#"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="img"></a>
            </div>
        </div>
    <?php endforeach; ?>

    <?php if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br /><?= $arResult["NAV_STRING"] ?>
    <?php endif; ?>
</div>