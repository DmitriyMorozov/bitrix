<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="review-block">
    <div class="review-text">
        <div class="review-text-cont">
            <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && ($arResult["FIELDS"]["PREVIEW_TEXT"] ?? '')):?>
                <h3><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></h3>
            <?endif;?>
            <?if($arParams["DISPLAY_DETAIL_TEXT"]!="N" && ($arResult["DETAIL_TEXT"] ?? '')):?>
                <p><?=$arResult["DETAIL_TEXT"];unset($arResult["DETAIL_TEXT"]);?></p>
            <?endif;?>
        </div>
        <div style="float: right; font-style: italic;">
            <?=$arResult["NAME"]?>
        </div>
    </div>
    <div style="clear: both;" class="review-img-wrap">
        <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
            <img
                    class="detail_picture"
                    border="0"
                    src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                    width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
                    height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
                    alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
                    title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
            />
        <?endif;?>
    </div>
</div>

