<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="sb_action">
    <?php if (!empty($arResult['DETAIL_PICTURE']) && is_array($arResult['DETAIL_PICTURE'])): ?>
        <a href=""><img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arResult['DETAIL_PICTURE']['ALT'] ?>"/></a>
    <?php endif; ?>
    <?php if ($arResult['NAME']): ?>
        <h4><?= $arResult['NAME'] ?></h4>
    <?php endif; ?>
    <?php if ($arResult['DETAIL_TEXT']): ?>
        <h5><a href=""><?= $arResult['DETAIL_TEXT'] ?></a></h5>
    <?php endif; ?>
    <a href="" class="sb_action_more">Подробнее &rarr;</a>
</div>

