<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php foreach ($arResult["ITEMS"] as $arItem) : ?>
<li>
<div class="rw_message">

    <?php if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])) : ?>
        <?php if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])) : ?>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                <img class="rw_avatar" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>"/>
            </a>
                <?php else: ?>
                    <img class="rw_avatar" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>"/>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]) : ?>
                <span class="rw_name">
                    <?= $arItem["NAME"]; ?>
                </span><br />
            <?php endif; ?>

            <?php if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]) : ?>
                <span class="rw_job">
                    <?= $arItem["PREVIEW_TEXT"]; ?>
                </span><br />
            <?php endif; ?>

            <?php if ($arParams["DISPLAY_DETAIL_TEXT"] != "N" && $arItem["DETAIL_TEXT"]) : ?>
                <p>
                    <?= $arItem["DETAIL_TEXT"]; ?>
                </p>
            <?php endif; ?>
    <div class="clearboth"></div>
    <div class="rw_arrow"></div>
</div>
</li>
    <?php endforeach; ?>
