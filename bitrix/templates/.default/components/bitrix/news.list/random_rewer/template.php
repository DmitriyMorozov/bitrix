<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="sb_reviewed">
    <?php
    if (!empty($arResult["ITEMS"])) {
        $randomKey = array_rand($arResult["ITEMS"]); // Получаем случайный ключ из массива
        $randomItem = $arResult["ITEMS"][$randomKey]; // Получаем случайный элемент из массива
        ?>
        <?php if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($randomItem["PREVIEW_PICTURE"])) : ?>
            <?php if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($randomItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])) : ?>
                <a href="<?= $randomItem["DETAIL_PAGE_URL"] ?>">
                    <img class="rw_avatar" src="<?= $randomItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $randomItem["NAME"] ?>"/>
                </a>
            <?php else: ?>
                <img class="rw_avatar" src="<?= $randomItem["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $randomItem["NAME"] ?>"/>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($arParams["DISPLAY_NAME"] != "N" && $randomItem["NAME"]) : ?>
            <span class="rw_name"><?= $randomItem["NAME"]; ?></span><br />
        <?php endif; ?>

        <?php if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $randomItem["PREVIEW_TEXT"]) : ?>
            <span class="rw_job"><?= $randomItem["PREVIEW_TEXT"]; ?></span><br />
        <?php endif; ?>

        <?php if ($arParams["DISPLAY_DETAIL_TEXT"] != "N" && $randomItem["DETAIL_TEXT"]) : ?>
            <p><?= $randomItem["DETAIL_TEXT"]; ?></p>
        <?php endif; ?>
    <?php } ?>
    <div class="clearboth"></div>
    <div class="sb_rw_arrow"></div>
</div>