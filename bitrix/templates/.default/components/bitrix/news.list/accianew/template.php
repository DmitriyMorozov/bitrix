<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div class="sb_action">
    <?php
    $lastItemKey = count($arResult["ITEMS"]) - 1;
    if (isset($arResult["ITEMS"][$lastItemKey])) {
        $arItem = $arResult["ITEMS"][$lastItemKey];
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="news-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <?php if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                <?php if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" style="float:left"/>
                    </a>
                <?php else: ?>
                    <img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" style="float:left"/>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                <?php if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                    <a href="<?php echo $arItem["DETAIL_PAGE_URL"] ?>"><b><?php echo $arItem["NAME"] ?></b></a><br />
                <?php else: ?>
                    <b><?php echo $arItem["NAME"] ?></b><br />
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                <?php echo $arItem["PREVIEW_TEXT"]; ?>
            <?php endif; ?>
        </div>
        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="sb_action_more">Подробнее</a>
    <?php } ?>
</div>